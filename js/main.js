$(document).ready(function(event){
    $('button').on('click', function(event){
        event.preventDefault();

        var url_da_api = "https://icuff17-api.herokuapp.com/teachers";
        var id_do_professor = $('input').val();
        var url_de_um_professor = url_da_api + '/' + id_do_professor;

        var requisicao_professor = $.ajax({
            url: url_de_um_professor
        });
        
        requisicao_professor.done(function(data) {
            var foto_do_professor = data.photo;

            $('#conteudo-professor').append('<div class="professor"><h2>' + data.name +'</h2><p>' + data.subject + '</p><img src="'+ foto_do_professor +'"></div>');
        });
        
        requisicao_professor.fail(function(data){
            alert("Erro! Professor com id " + id_do_professor + " não encontrado!");
        });
    });
});